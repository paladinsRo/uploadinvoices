import { Component, OnInit, Input } from '@angular/core';
import { ServiceCompanies } from 'src/app/services/company.service';
import { CompanyModel } from 'src/app/models/company.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { InvoiceModel } from 'src/app/models/invoice.model';

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.css']
})
export class CompanyComponent implements OnInit {
  public company: CompanyModel;
  inputForm: FormGroup;
  createInvoice: InvoiceModel = new InvoiceModel('','','','','','','','','','','','');


  constructor(private companyService: ServiceCompanies) { }

  ngOnInit() {
    this.company = this.companyService.getSelectedComapny();
    this.resetForm();
  }

  addNewInvoice(){
    this.companyService.addNewInvoiceToCompany(this.createInvoice);
    this.company = this.companyService.getSelectedComapny();
    this.resetForm();
  }

  resetForm(){
    this.createInvoice= new InvoiceModel('','','','','','','','','','','','');

    this.inputForm = new FormGroup({
      invoiceDate: new FormControl(null, Validators.required),
      invoiceNumber: new FormControl(null, Validators.required),
      customerId: new FormControl(null, Validators.required),
      billTo: new FormControl(null, Validators.required),
      subtotal: new FormControl(null, Validators.required),
      taxable: new FormControl(null, Validators.required),
      taxRate: new FormControl(null, Validators.required),
      taxDue: new FormControl(null, Validators.required),
      other: new FormControl(null, Validators.required),
      total: new FormControl(null, Validators.required),
      specialNotes: new FormControl(null, Validators.required),
      companyName: new FormControl(null, Validators.required),
    });
  }

}
