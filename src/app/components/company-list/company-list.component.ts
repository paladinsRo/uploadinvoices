import { Component, OnInit } from '@angular/core';
import { ServiceCompanies } from 'src/app/services/company.service';
import { CompanyModel } from 'src/app/models/company.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-company-list',
  templateUrl: './company-list.component.html',
  styleUrls: ['./company-list.component.css']
})
export class CompanyListComponent implements OnInit {
  public companies: CompanyModel[];


  constructor(
    private companySrv: ServiceCompanies,
    private router: Router
  ) { }

  ngOnInit() {
    this.companies = this.companySrv.getCompanies();

  }

  selectCompany(id){
    this.companySrv.setSelectedComapny(this.companySrv.getCompanyById(id)[0]);
    this.router.navigateByUrl('companySelected');

  }



}
