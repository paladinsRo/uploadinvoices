import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public inputForm: FormGroup;
  showError: Boolean = false;

  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    const user = this.inputForm.get('userName').value;
    const pass = this.inputForm.get('password').value;
    if ((user === 'user') && (pass === 'user')) {
      this.showError = false;
      this.router.navigateByUrl('companies');

    } else {
      this.showError = true;
    }
  }

}
