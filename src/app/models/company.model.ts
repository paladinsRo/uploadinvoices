import { InvoiceModel } from './invoice.model';

export class CompanyModel {
  id: string;
  companyName: string;
  invoices: InvoiceModel[];

  constructor(
    id: string,
    companyName: string,
    invoices: InvoiceModel[]

  ) {
    this.id = id;
    this.companyName = companyName;
    this.invoices = invoices;
  }
}
