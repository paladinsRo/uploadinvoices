export class InvoiceModel {
  id: string;
  invoiceDate: string;
  invoiceNumber: string;
  customerId: string;
  billTo: string;
  subtotal: string;
  taxable: string;
  taxRate: string;
  taxDue: string;
  other: string;
  total: string;
  specialNotes: string;
  companyName: string;

  constructor(
    invoiceDate: string,
    invoiceNumber: string,
    customerId: string,
    billTo: string,
    subtotal: string,
    taxable: string,
    taxRate: string,
    taxDue: string,
    other: string,
    total: string,
    specialNotes: string,
    companyName: string,
  ) {

    this.invoiceNumber = invoiceNumber;
    this.customerId = customerId;
    this.billTo = billTo;
    this.invoiceDate = invoiceDate;
    this.subtotal = subtotal;
    this.taxable = taxable;
    this.taxRate = taxRate;
    this.taxDue = taxDue;
    this.other = other;
    this.total = total;
    this.specialNotes = specialNotes;
    this.companyName = companyName;

  }
}
