import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { CompanyModel } from '../models/company.model';
import { InvoiceModel } from '../models/invoice.model';

@Injectable()
export class ServiceCompanies {

  public companies: CompanyModel[] = [new CompanyModel(null, null, null)];
  private selectedCompany: CompanyModel = new CompanyModel(null, null, null);
  constructor(
    private http: HttpClient) {
    this.onInit();
  }

  getCompanies() {
    return this.companies;
  }

  getCompanyById(id) {
    return this.companies.filter((x: CompanyModel) =>
      x.id === id
    );
  }

  getSelectedComapny() {
    return this.selectedCompany;
  }

  addNewInvoiceToCompany(invoice: InvoiceModel) {
    console.log(invoice)
    this.selectedCompany.invoices.push(invoice)
  }

  setSelectedComapny(comp: CompanyModel) {
    this.selectedCompany = comp;
  }


  onInit() {
    const invoicesMedin: InvoiceModel[] = [
      new InvoiceModel(
        '09/09/2019',
        '007782342',
        '00299324',
        'Blvd 1, NR 1, NY',
        '900',
        '100',
        '20',
        '09/30/2019',
        'Comment',
        '1000',
        'Test',
        'Accenture'
      )
      , new InvoiceModel(
        '03/09/2019',
        '2342341234',
        '0023423412',
        'Blvd 9, NR 2, WH',
        '3000',
        '100',
        '20',
        '09/30/2019',
        'Comment 2',
        '3100',
        'Test2',
        'UiPath'
      )
    ];
    this.companies.push(new CompanyModel('01', 'NewSoft', [new InvoiceModel('','','','','','','','','','','','')]));
    this.companies.push(new CompanyModel('02', 'Medinc', invoicesMedin));
    this.companies.push(new CompanyModel('03', 'ITSolutions', [new InvoiceModel('','','','','','','','','','','','')] ));


  }


}
